package com.quarta.android2.Items

import com.brandongogetap.stickyheaders.exposed.StickyHeader

class HeaderItem(val title: String) : com.quarta.android2.Items.Item(), StickyHeader
