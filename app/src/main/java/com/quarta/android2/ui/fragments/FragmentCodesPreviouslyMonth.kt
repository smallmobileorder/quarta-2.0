package com.quarta.android2.ui.fragments

import com.quarta.android2.model.Codes
import java.util.*


class FragmentCodesPreviouslyMonth : FragmentCodesThisMonth() {
    override fun filter(list: MutableList<Codes>): MutableList<Codes> {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val currentMonth = year * 12 + month + 1
        val previouslyMonth = currentMonth - 1
        return list.filter { it.getMonthFromCodes() == previouslyMonth }.toMutableList()
    }
}