package com.quarta.android2.ui.fragments

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.quarta.android2.R
import kotlinx.android.synthetic.main.fragment_share.*
import java.net.URLEncoder


class BottomNavigation(private val shareBody: String, private val key: String) :
    BottomSheetDialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_share, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val headerView = navigation_view.getHeaderView(0)
        val menu = navigation_view.menu

        menu.add(1, 1, Menu.NONE, null).setActionView(R.layout.navigation_item)
        menu.add(2, 2, Menu.NONE, null).setActionView(R.layout.navigation_item1)
        menu.add(3, 3, Menu.NONE, null).setActionView(R.layout.navigation_item2)
        menu.add(4, 4, Menu.NONE, null).setActionView(R.layout.navigation_item3)

        val navUsername = headerView.findViewById<View>(R.id.text_header) as TextView
        val headerText = "Код доступа Гостя $key"
        navUsername.text = headerText
        navigation_view.setNavigationItemSelectedListener { menuItem ->
            // Bottom Navigation Drawer menu item clicks
            when (menuItem.itemId) {
                1 -> {
                    val sdk = android.os.Build.VERSION.SDK_INT
                    if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        val clipboard =
                            activity!!.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
                        clipboard.text = shareBody
                        dismiss()
                    } else {
                        val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip = ClipData.newPlainText("", shareBody)
                        clipboard.primaryClip = clip
                        dismiss()
                    }
                }
                2 -> {
                    val uri = Uri.parse("smsto:")
                    val it = Intent(Intent.ACTION_SENDTO, uri)
                    it.putExtra("sms_body", shareBody)
                    startActivity(it)
                    dismiss()
                }
                3 -> {
                    val intent = Intent(Intent.ACTION_VIEW)
                    val url = "https://api.whatsapp.com/send?text=" + URLEncoder.encode(
                        shareBody,
                        "UTF-8"
                    )
                    intent.putExtra(Intent.EXTRA_TEXT, shareBody)
                    intent.type = "text/plain"
                    intent.setPackage("com.whatsapp")
                    intent.data = Uri.parse(url)
                    dismiss()
                    startActivity(intent)
                }
                4 -> {
                    //cancel
                    dismiss()
                }
            }
            true
        }
    }
}