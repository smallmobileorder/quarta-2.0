package com.quarta.android2.ui.fragments

import android.annotation.SuppressLint
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.R
import com.quarta.android2.model.Home
import com.quarta.android2.model.User
import kotlinx.android.synthetic.main.item_recycler_room_for_access.view.*


internal class AdapterRoomsForAccess(
    var listOwnerRooms: MutableList<Home>,
    var user: User,
    private val activity: com.quarta.android2.MainActivity
) :
    RecyclerView.Adapter<AdapterRoomsForAccess.BaseViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val viewHolder: BaseViewHolder
        val view =
            from(parent.context).inflate(R.layout.item_recycler_room_for_access, parent, false)
        viewHolder = BaseViewHolder(view)
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = listOwnerRooms[position]
        val view = holder.itemView
        view.btn_analytics.setOnCheckedChangeListener { _, _ -> }
        view.btn_analytics.isChecked = user.listFlats.contains(item.id)
        view.textNumber.text = item.boxNumber.toString()
        val textSize =
            activity.applicationContext.resources.getDimensionPixelSize(R.dimen.size_text_large) /
                    activity.applicationContext.resources.displayMetrics.scaledDensity
        view.btn_analytics.textSize = when {
            item.address.toString().length > 75 -> textSize / 2.25f
            item.address.toString().length > 50 -> textSize / 1.5f
            else -> textSize
        }
        view.btn_analytics.text = item.address.toString()
        view.btn_analytics.setOnCheckedChangeListener { _, isChecked ->
            var ref = FirebaseDatabase.getInstance().reference
            ref = ref.child("users")
                .child(user.id)
                .child("flats")
                .child(item.id)
            if (isChecked) {
                ref.setValue(1)
            } else {
                ref.removeValue()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return listOwnerRooms.size
    }


    internal open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
