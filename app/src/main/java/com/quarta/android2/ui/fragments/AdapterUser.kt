package com.quarta.android2.ui.fragments

import android.annotation.SuppressLint
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.quarta.android2.R
import com.quarta.android2.model.User
import kotlinx.android.synthetic.main.item_recycler_personal.view.*


internal class AdapterUser(
    var data: MutableList<User>,
    private val activity: com.quarta.android2.MainActivity
) :
    RecyclerView.Adapter<AdapterUser.BaseViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val viewHolder: BaseViewHolder
        val view = from(parent.context).inflate(R.layout.item_recycler_personal, parent, false)
        viewHolder = MyViewHolder(view)
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = data[position]
        if (holder is MyViewHolder) {
            val view = holder.itemView
            view.textPosition.text = item.position
            view.textEmail.text = item.email
            view.textName.text = item.name
            view.itemCard.setOnClickListener {
                activity.resultFragment =
                    FragmentAccessLevel(item)
                activity.replaceFragment()
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return data.size
    }

    private class MyViewHolder internal constructor(itemView: View) : BaseViewHolder(itemView)

    internal open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
