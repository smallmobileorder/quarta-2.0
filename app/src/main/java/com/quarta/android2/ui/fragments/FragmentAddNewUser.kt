package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.R
import com.quarta.android2.emailCorrect
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_add_new_user.*


class FragmentAddNewUser : Fragment() {


    companion object {
        fun newInstance() = FragmentAddNewUser()
    }

    private lateinit var mainActivity: com.quarta.android2.MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as com.quarta.android2.MainActivity
        return inflater.inflate(R.layout.fragment_add_new_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_personal_btn?.setOnClickListener {
            val email = emailTextInputEditText?.text.toString()
            val position = positionTextInputEditText?.text.toString()
            val me = (mainActivity.application as com.quarta.android2.App).currentUser
            when {
                !email.emailCorrect() ->
                    Snackbar.make(
                        main_container,
                        getString(R.string.non_correct_string),
                        Snackbar.LENGTH_SHORT
                    ).show()
                !userExist() -> Snackbar.make(
                    main_container,
                    getString(R.string.user_not_exist),
                    Snackbar.LENGTH_LONG
                ).show()
                position.isEmpty() -> Snackbar.make(
                    main_container,
                    getString(R.string.position_is_empty),
                    Snackbar.LENGTH_LONG
                ).show()
                email == me.email -> {
                    Snackbar.make(
                        main_container,
                        getString(R.string.it_you_email),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                else -> {
                    mainActivity.getListUserFromFireBase(listenerSuccess = {
                        val user = it.find { user -> user.email == email }
                        if (user != null) {
                            me.listStaffs[user.id] = position
                            user.ownerId = me.id
                            val ref = FirebaseDatabase.getInstance().reference
                            ref.child("users")
                                .child(me.id)
                                .child("staff")
                                .child(user.id)
                                .setValue(position)
                            ref.child("users")
                                .child(user.id)
                                .child("ownerId")
                                .setValue(me.id)
                            mainActivity.resultFragment =
                                FragmentAccessLevel(user)
                            mainActivity.replaceFragment()
                        } else {
                            if (add_personal_btn != null)
                                Snackbar.make(
                                    add_personal_btn,
                                    getString(R.string.user_not_found),
                                    Snackbar.LENGTH_SHORT
                                ).show()
                        }
                    }, listenerFailed = {
                        Snackbar.make(container, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
                    })
                }
            }
        }
        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
    }

    private fun userExist() = true


}
