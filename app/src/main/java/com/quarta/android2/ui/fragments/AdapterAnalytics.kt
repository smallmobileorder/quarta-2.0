package com.quarta.android2.ui.fragments

import android.annotation.SuppressLint
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.quarta.android2.R
import com.quarta.android2.model.Analytics
import kotlinx.android.synthetic.main.item_recycler_analytics.view.*


internal class AdapterAnalytics(
    var data: MutableList<Analytics>,
    private val activity: com.quarta.android2.MainActivity
) :
    RecyclerView.Adapter<AdapterAnalytics.BaseViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val viewHolder: BaseViewHolder
        val view = from(parent.context).inflate(R.layout.item_recycler_analytics, parent, false)
        viewHolder = MyViewHolder(view)
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = data[position]
        if (item.address != null)
            when (holder) {
                is MyViewHolder -> {
                    val view = holder.itemView
                    view.textTypeChange.text =
                        activity.getString(
                            if (item.isOpen) R.string.door_open
                            else R.string.door_close
                        )
                    view.textPosition.text = item.address.toString()
                    view.textName.text = item.name.toString()
                    view.textData.text = item.date
                    view.textNumber.text = item.boxNumber.toString()
                }
            }
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return data.size
    }

    private class MyViewHolder internal constructor(itemView: View) : BaseViewHolder(itemView)

    internal open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
