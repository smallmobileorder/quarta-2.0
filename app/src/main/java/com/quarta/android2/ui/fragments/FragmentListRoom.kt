package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.brandongogetap.stickyheaders.StickyLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.converToVisible
import com.quarta.android2.model.Home
import kotlinx.android.synthetic.main.fragment_list_room.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FragmentListRoom : Fragment() {
    private lateinit var mainActivity: com.quarta.android2.MainActivity
    private lateinit var adapterRecycler: AdapterRoom
    private var currentListRoom = listOf<Home>()
    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mainActivity.getListRoomFromFireBase(
            listenerSuccess = { list ->
                GlobalScope.launch(Dispatchers.Main) {
                    currentListRoom = list
                    initToolbar()
                    updateRecycler(list)
                    pullUpRefresh?.isRefreshing = false
                }
            },
            listenerFailed = {
                pullUpRefresh?.isRefreshing = false
                if (pullUpRefresh != null)
                    Snackbar.make(pullUpRefresh, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_list_room, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as com.quarta.android2.MainActivity
        initListeners()
        initToolbar()
        initRecycler()
        pullUpRefresh?.isRefreshing = true
        refreshListener.onRefresh()
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)

    }

    private fun initToolbar() {
        val currentUser = (mainActivity.application as com.quarta.android2.App).currentUser
        val menu = toolbar?.menu
        menu?.findItem(R.id.btn_cleaning)?.isVisible = true
        menu?.findItem(R.id.btn_list_codes)?.isVisible = currentUser.isAnalyticEnabled
        menu?.findItem(R.id.btn_list_analytics)?.isVisible = currentUser.isAnalyticEnabled
        menu?.findItem(R.id.btn_list_personal)?.isVisible = currentUser.isStaffEnabled
    }

    override fun onResume() {
        super.onResume()
        searchTextInput?.isFocusable = false
    }

    private fun initListeners() {
        pullUpRefresh?.setOnRefreshListener(refreshListener)
        searchTextInput?.setOnClickListener {
            searchTextInput?.isIconified = false
        }
        searchTextInput?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(s: String?): Boolean {
                if (s != null)
                    updateRecycler(currentListRoom.filter {
                        (it.address != null && it.address!!.contains(s, true))
                                || (it.boxNumber!!.toString().contains(s, true))
                    })
                return true
            }
        })
        toolbar?.setNavigationOnClickListener { logOutDialog() }
        toolbar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.btn_list_personal -> {
                    mainActivity.resultFragment =
                        FragmentUsers.newInstance()
                    mainActivity.replaceFragment()
                }
                R.id.btn_cleaning -> {
                    mainActivity.resultFragment =
                        FragmentCleaning.newInstance()
                    mainActivity.replaceFragment()
                }
                R.id.btn_list_analytics -> {
                    mainActivity.resultFragment =
                        FragmentAnalytics.newInstance()
                    mainActivity.replaceFragment()
                }
                R.id.btn_list_codes -> {
                    mainActivity.resultFragment =
                        FragmentListCodes.newInstance()
                    mainActivity.replaceFragment()
                }
            }
            true
        }
    }


    private fun initRecycler() {
        val data = mutableListOf<com.quarta.android2.Items.Item>()
        adapterRecycler = AdapterRoom(data, mainActivity)
        val layoutManager = StickyLayoutManager(context, adapterRecycler)
        layoutManager.elevateHeaders(true)
        recycler_view?.adapter = adapterRecycler
        recycler_view?.layoutManager = layoutManager
    }


    private fun updateRecycler(list: List<Home>) {
        GlobalScope.launch(Dispatchers.Main) {
            val data = mutableListOf<com.quarta.android2.Items.Item>()
            withContext(Dispatchers.IO) {
                val listEmpty = list.filter { !it.roomIsNotEmpty() }
                val listNotEmpty = list.filter { it.roomIsNotEmpty() }
                if (listNotEmpty.isNotEmpty()) {
                    data.add(com.quarta.android2.Items.HeaderItem("Занятые"))
                }
                listNotEmpty.forEach {
                    data.add(com.quarta.android2.Items.Item(it))
                }
                if (listEmpty.isNotEmpty()) {
                    data.add(com.quarta.android2.Items.HeaderItem("Свободные"))
                }
                listEmpty.forEach {
                    data.add(com.quarta.android2.Items.Item(it))
                }
                adapterRecycler.data = data
            }
            under_recycler_text?.visibility = data.isEmpty().converToVisible()
            adapterRecycler.notifyDataSetChanged()
        }
    }


    private fun logOutDialog() {
        AlertDialog.Builder(context!!).setMessage("Выйти из аккаунта?")
            .setPositiveButton("Выйти") { dialog, _ ->
                mainActivity.logOut()
                dialog.dismiss()
            }
            .setNeutralButton("Отмена") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }


    companion object {
        fun newInstance() = FragmentListRoom()

    }

}
