package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.emailCorrect
import kotlinx.android.synthetic.main.fragment_login.*


class FragmentLogin : Fragment() {

    companion object {
        fun newInstance() = FragmentLogin()
    }

    private lateinit var mainActivity: com.quarta.android2.MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as com.quarta.android2.MainActivity
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        new_sign_in_btn?.setOnClickListener {
            mainActivity.resultFragment = FragmentSignIn.newInstance()
            mainActivity.replaceFragment()
        }

        emailTextInputEditText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                p0!!.trim()
                if (p0.isNullOrEmpty() || searchTextInputEditText?.text.toString().isEmpty())
                    button_add_home?.visibility = View.GONE
                else
                    button_add_home?.visibility = View.VISIBLE
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
        searchTextInputEditText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                p0!!.trim()
                if (p0.isNullOrEmpty() || emailTextInputEditText?.text.toString().isEmpty())
                    button_add_home?.visibility = View.GONE
                else
                    button_add_home?.visibility = View.VISIBLE

            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
        button_add_home?.setOnClickListener {
            if (emailTextInputEditText?.text.toString().emailCorrect()) {
                disableView()
                mainActivity.tryToAuth(
                    emailTextInputEditText?.text.toString(),
                    searchTextInputEditText?.text.toString()
                ) {
                   enableView()
                }
            } else
                Snackbar.make(
                    main_container,
                    getString(R.string.non_correct_string),
                    Snackbar.LENGTH_SHORT
                ).show()
        }
    }
    private fun enableView() {
        button_add_home?.isEnabled = true
        button_add_home?.alpha = 1f
        progress_circular.visibility = View.GONE
    }

    private fun disableView() {
        button_add_home?.isEnabled = false
        button_add_home?.alpha = 0.5f
        progress_circular.visibility = View.VISIBLE
    }

}
