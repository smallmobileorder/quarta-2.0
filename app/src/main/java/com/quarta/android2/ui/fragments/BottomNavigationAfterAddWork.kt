package com.quarta.android2.ui.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.quarta.android2.R
import kotlinx.android.synthetic.main.fragment_share.*


class BottomNavigationAfterAddWork :
    BottomSheetDialogFragment() {
    var isClicked = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_share, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isClicked = false
        val headerView = navigation_view.getHeaderView(0)
        val menu = navigation_view.menu
        menu.add(1, 1, Menu.NONE, null).setActionView(R.layout.navigation_item3)
        val textViewCancel =
            menu.getItem(0).actionView.findViewById<View>(R.id.text_item_navigation) as TextView
        textViewCancel.text = "Готово"

        val navUsername = headerView.findViewById<View>(R.id.text_header) as TextView
        val headerText = "Спасибо, наш менеджер свяжется с вами в ближайшее время"
        navUsername.text = headerText
        navigation_view.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                1 -> {
                    isClicked = true
                    listener.invoke(true)
                    dismiss()
                }
            }
            true
        }
    }


    override fun onDismiss(dialog: DialogInterface) {

        if (!isClicked) listener.invoke(false)
        super.onDismiss(dialog)

    }

    var listener: ((result: Boolean) -> Unit) = {}

}