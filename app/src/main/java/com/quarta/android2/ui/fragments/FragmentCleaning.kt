package com.quarta.android2.ui.fragments

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.R
import com.quarta.android2.convertLongToTime
import com.quarta.android2.model.Service
import com.quarta.android2.model.ServiceType.*
import kotlinx.android.synthetic.main.fragment_list_cleaning.*


open class FragmentCleaning : Fragment() {

    private lateinit var mainActivity: com.quarta.android2.MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as com.quarta.android2.MainActivity
        return inflater.inflate(R.layout.fragment_list_cleaning, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() {
        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
        if (list_rb_cleaning != null) {
            val count: Int = list_rb_cleaning.childCount
            for (i in 0 until count) {
                val o: View = list_rb_cleaning.getChildAt(i)
                if (o is RadioButton) {
                    o.setOnCheckedChangeListener { view, isChecked ->
                        showCreateWorkBtn()
                        if (isChecked) {
                            animateColor(
                                view,
                                resources.getColor(R.color.color_text),
                                resources.getColor(R.color.color_text_grey)
                            )
                        } else {
                            animateColor(
                                view,
                                resources.getColor(R.color.color_text_grey),
                                resources.getColor(R.color.color_text)
                            )
                        }
                    }
                }
            }
        }
    }

    private fun showCreateWorkBtn() {
        createWorkBtn.animate().alpha(1f).start()
        createWorkBtn.setOnClickListener {
            val bottomNavDrawerFragment =
                BottomNavigationOpenClose(
                    "Да",
                    "Вы уверены что хотите заказать услугу?",
                    "Нет"
                )
            bottomNavDrawerFragment.show(
                mainActivity.supportFragmentManager,
                bottomNavDrawerFragment.tag
            )
            bottomNavDrawerFragment.listener = {
                if (it) {
                    val ref = FirebaseDatabase.getInstance().reference
                    val me = (mainActivity.application as com.quarta.android2.App).currentUser
                    val serviceType = when (list_rb_cleaning.checkedRadioButtonId) {
                        R.id.rb_cleaning -> CLEANING
                        R.id.rb_textil -> TEXTIL
                        R.id.rb_laundry -> LAUNDRY
                        R.id.rb_repair -> REPAIR
                        R.id.rb_CCTV -> CCTV
                        else -> KEYLESSACCESS
                    }
                    val service = Service(
                        email = me.email,
                        isDone = false,
                        ownerId = me.ownerId,
                        serviceType = serviceType,
                        date = System.currentTimeMillis().convertLongToTime()
                    )
                    ref.child("services").push()
                        .setValue(service.toMap()).addOnSuccessListener {
                            val bottomNavDrawerAfterFragment =
                                BottomNavigationAfterAddWork()
                            bottomNavDrawerAfterFragment.show(
                                mainActivity.supportFragmentManager,
                                bottomNavDrawerAfterFragment.tag
                            )
                            bottomNavDrawerAfterFragment.listener = { isDone ->
                                if (isDone) {
                                    mainActivity.onBackPressed()
                                }
                            }
                        }
                }
            }
        }
    }

    private fun animateColor(
        v: View?,
        endColor: Int,
        startColor: Int
    ) {
        val valueAnimator: ValueAnimator = ObjectAnimator.ofInt(
            v,
            "textColor",
            startColor,
            endColor
        )
        valueAnimator.setEvaluator(ArgbEvaluator())
        valueAnimator.duration = 100
        valueAnimator.start()
    }

    companion object {
        fun newInstance() = FragmentCleaning()
    }
}
