package com.quarta.android2.ui.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.quarta.android2.R
import kotlinx.android.synthetic.main.fragment_close_open.*


class BottomNavigationOpenClose(
    private val openOrClose: String,
    private val textForOpenOrClose: String,
    private val textForCancel: String = "Отмена"
) :
    BottomSheetDialogFragment() {

    var isClicked = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_close_open, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isClicked = false
        val headerView = navigation_view_top.getHeaderView(0)
        val navUsername = headerView.findViewById<View>(R.id.text_header) as TextView
        val headerText = textForOpenOrClose
        val menuHeader = navigation_view_top.menu
        menuHeader.add(1, 1, Menu.NONE, null).setActionView(R.layout.navigation_item4)
        val textView =
            menuHeader.getItem(0).actionView.findViewById<View>(R.id.text_item_navigation) as TextView
        textView.text = textForCancel
        val menu = navigation_view_cancel.menu
        menu.add(1, 1, Menu.NONE, null).setActionView(R.layout.navigation_item3)
        val textViewCancel =
            menu.getItem(0).actionView.findViewById<View>(R.id.text_item_navigation) as TextView
        textViewCancel.text = openOrClose
        navUsername.text = headerText
        navigation_view_cancel.setNavigationItemSelectedListener { menuItem ->
            // Bottom Navigation Drawer menu item clicks
            when (menuItem.itemId) {
                1 -> {
                    //cancel
                    isClicked = true
                    listener.invoke(true)
                    dismiss()
                }

            }
            true
        }
        navigation_view_top.setNavigationItemSelectedListener { menuItem ->
            // Bottom Navigation Drawer menu item clicks
            when (menuItem.itemId) {
                1 -> {
                    //cancel

                    isClicked = true
                    listener.invoke(false)
                    dismiss()
                }
            }
            true
        }
    }

    override fun onDismiss(dialog: DialogInterface) {

        if (!isClicked) listener.invoke(false)
        super.onDismiss(dialog)

    }

    var listener: ((result: Boolean) -> Unit) = {}
}