package com.quarta.android2.ui.fragments

import android.annotation.SuppressLint
import android.content.Context.VIBRATOR_SERVICE
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.brandongogetap.stickyheaders.exposed.StickyHeader
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.R
import com.quarta.android2.convertLongToTime
import com.quarta.android2.model.Analytics
import com.quarta.android2.model.Home
import kotlinx.android.synthetic.main.fragment_list_room.*
import kotlinx.android.synthetic.main.item_recycler_room.view.*
import kotlinx.android.synthetic.main.item_recycler_top.view.*


internal class AdapterRoom(
    var data: MutableList<com.quarta.android2.Items.Item>,
    private val activity: com.quarta.android2.MainActivity
) :
    RecyclerView.Adapter<AdapterRoom.BaseViewHolder>(),
    StickyHeaderHandler {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val viewHolder: BaseViewHolder
        viewHolder = when (viewType) {
            0 -> {
                val view = from(parent.context).inflate(R.layout.item_recycler_room, parent, false)
                MyViewHolder(view)
            }
            else -> {
                val view = from(parent.context).inflate(R.layout.item_recycler_top, parent, false)
                MyOtherViewHolder(view)
            }
        }
        return viewHolder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = data[position]
        when (holder) {
            is MyViewHolder -> {
                val currentRoom = item.date!!
                val view = holder.itemView
                if (!currentRoom.roomIsNotEmpty()) {
                    view.textName.text = "Квартира свободна"
                    view.textEmail.text = "Нажмите, чтобы настроить"
                } else {
                    view.textEmail.text = "${currentRoom.startDate} - ${currentRoom.endDate}"
                    view.textName.text = "Код доступа Гостя ${currentRoom.codeHome}"
                }
                view.textPosition.text = currentRoom.address.toString()
                view.textNumber.text = currentRoom.boxNumber.toString()
                view.itemCard.setOnClickListener {
                    if ((activity.application as com.quarta.android2.App).currentUser.isFlatEditEnabled) {
                        activity.resultFragment =
                            FragmentCurrentRoom.newInstance(currentRoom)
                        activity.replaceFragment()
                    }
                }
                view.switch_state.setOnCheckedChangeListener { _, _ -> }
                view.switch_state.isChecked = currentRoom.isOpen
                view.switch_state.setOnCheckedChangeListener { _, hasCheck ->
                    onCheckedChanged(currentRoom, hasCheck, view)
                }
            }
            is MyOtherViewHolder -> {
                holder.itemView.textHeader.text = (item as com.quarta.android2.Items.HeaderItem).title
            }
        }
    }

    private fun onCheckedChanged(currentRoom: Home, hasCheck: Boolean, view: View) {
        vibrate()
        try {
            val bottomNavDrawerFragment =
                BottomNavigationOpenClose(
                    if (hasCheck) "Открыть" else "Закрыть",
                    if (hasCheck) "Вы уверены, что хотите Открыть квартиру?" else "Вы уверены, что хотите Закрыть квартиру?"
                )
            bottomNavDrawerFragment.show(
                activity.supportFragmentManager,
                bottomNavDrawerFragment.tag
            )
            bottomNavDrawerFragment.listener = {
                if (it) {
                    currentRoom.isOpen = hasCheck
                    currentRoom.isEmpty = !hasCheck
                    val intBoolInvert = if (hasCheck) 1 else 0
                    var text = "Квартира "
                    text += if (hasCheck) "открыта" else "закрыта"
                    val me = (activity.application as com.quarta.android2.App).currentUser
                    val time = System.currentTimeMillis()
                    val ref = FirebaseDatabase.getInstance().reference
                    val analytics = Analytics(
                        name = me.name,
                        address = currentRoom.address.toString(),
                        boxNumber = currentRoom.boxNumber!!,
                        flatId = currentRoom.id,
                        isOpen = hasCheck
                    )
                    analytics.date = time.convertLongToTime()

                    ref.child("tasks").child("landlordDoorsStatus")
                        .child(me.ownerId)
                        .child(currentRoom.id)
                        .setValue(hasCheck)

                    ref.child("tasks").child(currentRoom.id).child("lock")
                        .child("status")
                        .setValue(intBoolInvert)

                    ref
                        .child("users")
                        .child(me.ownerId)
                        .child("analytic")
                        .push()
                        .setValue(analytics.toMap())

                    if (activity.pullUpRefresh != null) {
                        Snackbar.make(
                            activity.pullUpRefresh,
                            text,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }

                } else {
                    view.switch_state.setOnCheckedChangeListener { _, _ -> }
                    view.switch_state.isChecked = !view.switch_state.isChecked
                    view.switch_state.setOnCheckedChangeListener { _, hasCheck ->
                        onCheckedChanged(currentRoom, hasCheck, view)
                    }
                }
            }
        } catch (ignoredException: Exception) {
        }
    }

    private fun vibrate() {
        val v = activity.getSystemService(VIBRATOR_SERVICE) as Vibrator?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v!!.vibrate(
                VibrationEffect.createOneShot(
                    20,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            )
        } else {
            //deprecated in API 26
            v!!.vibrate(20)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is StickyHeader)
            1 else 0
    }

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getAdapterData(): List<*> {
        return data
    }

    private class MyViewHolder internal constructor(itemView: View) : BaseViewHolder(itemView)

    private class MyOtherViewHolder internal constructor(itemView: View) : BaseViewHolder(itemView)

    internal open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
