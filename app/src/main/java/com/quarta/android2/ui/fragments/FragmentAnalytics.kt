package com.quarta.android2.ui.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.converToVisible
import com.quarta.android2.model.Analytics
import kotlinx.android.synthetic.main.fragment_list_personal.*


class FragmentAnalytics : Fragment() {
    private lateinit var mainActivity: com.quarta.android2.MainActivity
    private lateinit var adapterRecycler: AdapterAnalytics
    private var currentListPersonal = listOf<Analytics>()
    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mainActivity.getListAnalyticsFromFireBase(
            listenerSuccess = { list ->
                currentListPersonal = list
                updateRecycler(list)
                pullUpRefresh?.isRefreshing = false
            },
            listenerFailed = {
                pullUpRefresh?.isRefreshing = false
                if (pullUpRefresh != null)
                    Snackbar.make(pullUpRefresh, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_analytics, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as com.quarta.android2.MainActivity
        initRecycler()
        initListeners()
        pullUpRefresh?.isRefreshing = true
        refreshListener.onRefresh()
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)

    }


    private fun initListeners() {
        pullUpRefresh?.setOnRefreshListener(refreshListener)
        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
    }


    private fun initRecycler() {
        val data = mutableListOf<Analytics>()
        adapterRecycler = AdapterAnalytics(data, mainActivity)
        val layoutManager = LinearLayoutManager(context)
        recycler_view?.layoutManager = layoutManager
        recycler_view?.adapter = adapterRecycler
    }


    private fun updateRecycler(list: List<Analytics>) {
        adapterRecycler.data = list.toMutableList()
        under_recycler_text?.visibility = list.isEmpty().converToVisible()
        adapterRecycler.notifyDataSetChanged()
    }


    companion object {
        fun newInstance() = FragmentAnalytics()
    }

}
