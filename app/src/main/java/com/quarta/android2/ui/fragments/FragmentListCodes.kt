package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.quarta.android2.R
import kotlinx.android.synthetic.main.fragment_list_codes.*


class FragmentListCodes : Fragment() {
    private lateinit var mainActivity: com.quarta.android2.MainActivity
    private val leftFragment = FragmentCodesPreviouslyMonth()
    private val rightFragment = FragmentCodesThisMonth()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_list_codes, container, false)
    }

    private fun initViewPager() {
        view_pager?.offscreenPageLimit = 1
        configureTabLayout()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as com.quarta.android2.MainActivity
        initViewPager()
        initListeners()
    }

    private fun configureTabLayout() {
        val adapter = TabPagerAdapter(
            childFragmentManager,
            2
        )
        view_pager?.adapter = adapter
        view_pager?.currentItem = 1
        tab_layout_list_codes?.setupWithViewPager(view_pager)
    }

    inner class TabPagerAdapter(fm: FragmentManager, private var tabCount: Int) :
        FragmentPagerAdapter(fm) {
        private val tabTitles = arrayOf("За прошлый месяц", "За этот месяц")

        override fun getItem(position: Int): Fragment {

            val fragment = when (position) {
                0 -> leftFragment
                else -> rightFragment
            }
            fragment.mainFragment = this@FragmentListCodes
            return fragment
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            // генерируем заголовок в зависимости от позиции
            return tabTitles[position]
        }

    }

    private fun initListeners() {
        view_pager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                updateNumberText()
            }

        })

        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
    }

    fun updateNumberText() {
        val fragment = when (view_pager?.currentItem) {
            0 -> leftFragment
            else -> rightFragment
        }
        text_list_codes_number?.text = fragment.getSizeList()
    }

    companion object {
        fun newInstance() = FragmentListCodes()
    }

}
