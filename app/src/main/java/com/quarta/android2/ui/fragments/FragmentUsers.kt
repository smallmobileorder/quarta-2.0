package com.quarta.android2.ui.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.converToVisible
import com.quarta.android2.model.User
import kotlinx.android.synthetic.main.fragment_list_personal.*


class FragmentUsers : Fragment() {
    private lateinit var mainActivity: com.quarta.android2.MainActivity
    private lateinit var adapterRecycler: AdapterUser
    private var currentListPersonal = listOf<User>()
    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mainActivity.getListUserFromFireBase(
            listenerSuccess = { list ->
                currentListPersonal = list
                updateRecycler(list)
                pullUpRefresh?.isRefreshing = false
            },
            listenerFailed = {
                pullUpRefresh?.isRefreshing = false
                if (pullUpRefresh != null)
                    Snackbar.make(pullUpRefresh, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_list_personal, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as com.quarta.android2.MainActivity
        initRecycler()
        initListeners()
        pullUpRefresh?.isRefreshing = true
        refreshListener.onRefresh()
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)

    }


    private fun initListeners() {
        pullUpRefresh?.setOnRefreshListener(refreshListener)
        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
        toolbar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.add_person -> {
                    mainActivity.resultFragment =
                        FragmentAddNewUser.newInstance()
                    mainActivity.replaceFragment()
                }
            }
            true
        }
    }


    private fun initRecycler() {
        val data = mutableListOf<User>()
        adapterRecycler = AdapterUser(data, mainActivity)
        val layoutManager = LinearLayoutManager(context)
        recycler_view?.layoutManager = layoutManager
        recycler_view?.adapter = adapterRecycler
    }


    private fun updateRecycler(list: MutableList<User>) {
        val currentUser = (mainActivity.application as com.quarta.android2.App).currentUser
        val result = list.filter { currentUser.listStaffs.containsKey(it.id) }
        result.forEach {
            it.position = currentUser.listStaffs[it.id]!!
        }
        under_recycler_text?.visibility = result.isEmpty().converToVisible()
        adapterRecycler.data = result.toMutableList()
        adapterRecycler.notifyDataSetChanged()
    }


    companion object {
        fun newInstance() = FragmentUsers()
    }

}
