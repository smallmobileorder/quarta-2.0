package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.emailCorrect
import com.quarta.android2.model.User
import kotlinx.android.synthetic.main.fragment_sign_in.*


class FragmentSignIn : Fragment() {


    companion object {
        fun newInstance() = FragmentSignIn()
    }

    private lateinit var mainActivity: com.quarta.android2.MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as com.quarta.android2.MainActivity
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_add_home?.setOnClickListener {
            val name = nameTextInputEditText?.text.toString()
            val email = emailTextInputEditText?.text.toString()
            val password = searchTextInputEditText?.text.toString()
            when {
                name.isEmpty() -> Snackbar.make(
                    main_container,
                    "Не указано ваше имя",
                    Snackbar.LENGTH_LONG
                ).show()
                !email.emailCorrect() ->
                    Snackbar.make(
                        main_container,
                        getString(R.string.non_correct_string),
                        Snackbar.LENGTH_SHORT
                    ).show()
                password.length < 8 -> Snackbar.make(
                    main_container,
                    getString(R.string.veru_shot_key),
                    Snackbar.LENGTH_LONG
                ).show()
                else -> {
                    disableView()
                    val user = User("", email = email, password = password, name = name)
                    mainActivity.сreateUserUsingEmailAndPassowrd(user) {
                        println(it)
                        if (it) {
                            mainActivity.tryToAuth(user.email, user.password) {
                                enableView()
                            }
                        } else {
                            enableView()
                            Snackbar.make(
                                main_container,
                                getString(R.string.failed_sign_in),
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            }
        }

        sign_in_back_btn?.setOnClickListener {
            mainActivity.onBackPressed()
        }
    }


    private fun enableView() {
        button_add_home?.isEnabled = true
        button_add_home?.alpha = 1f
        progress_circular.visibility = View.GONE
    }

    private fun disableView() {
        button_add_home?.isEnabled = false
        button_add_home?.alpha = 0.5f
        progress_circular.visibility = View.VISIBLE
    }

}
