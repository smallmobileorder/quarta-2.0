package com.quarta.android2.ui.fragments

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.*
import com.quarta.android2.model.Codes
import com.quarta.android2.model.Home
import com.quarta.android2.model.Home.Companion.DEFAULT_STRING_DATA
import kotlinx.android.synthetic.main.fragment_current_room.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import java.util.*


class FragmentCurrentRoom(private val home: Home) : Fragment() {

    private lateinit var mainActivity: MainActivity
    private var firstEditableFieldFocused = true
    private var firstEditableFieldFocusedTime = 0L
    private var previouslyCode: Int? = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as MainActivity
        return inflater.inflate(R.layout.fragment_current_room, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as MainActivity
        setDataFromHome()
        notifyStateButton()
        initListeners()
    }

    private fun setDataFromHome() {
        previouslyCode = home.codeHome
        updateToolbarCode()
        dataStartTextInputEditText?.setText(home.startDate)
        dataEndTextInputEditText?.setText(home.endDate)
        addressTextInputEditText?.setText(home.address)
        descriptionTextInputEditText?.setText(home.description)
    }

    private fun updateToolbarCode() {
        if (!home.roomIsNotEmpty())
            toolbar?.title = "Свободна"
        else
            toolbar?.title = home.codeHome.toString()
    }

    private val listenerTextWatcher = object : TextWatcher {
        var a = ""
        override fun afterTextChanged(s: Editable?) {
            if (a != s.toString()) {
                notifyChange()
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            a = s.toString()
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    private fun showCalendar(editText: EditText, minTime: Long) {
        val currentDate = Calendar.getInstance()
        val date = Calendar.getInstance()
        val dialog = DatePickerDialog(
            context!!,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                date.set(year, monthOfYear, dayOfMonth)
                val timeDialog = TimePickerDialog(
                    context,
                    TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        date.set(Calendar.MINUTE, minute)
                        editText.setText(convertCalendarToData(date))
                        notifyChange()
                        notifyStateButton()
                        if (dataStartTextInputEditText.text.toString().convertDateToLong() > (dataEndTextInputEditText.text.toString().convertDateToLong() - 1000 * 60)) {
                            Toast.makeText(
                                context,
                                "Дата окончания аренды должна быть больше даты начала аренды",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                            dataEndTextInputEditText.setText(dataStartTextInputEditText.text.toString())
                        }

                    },
                    currentDate.get(Calendar.HOUR_OF_DAY),
                    currentDate.get(Calendar.MINUTE),
                    true
                )

                timeDialog.show()
            },
            currentDate.get(Calendar.YEAR),
            currentDate.get(Calendar.MONTH),
            currentDate.get(Calendar.DATE)
        )
        dialog.datePicker.minDate = minTime
        dialog.show()
    }

    private fun notifyStateButton() {
        notify_visitor?.isEnabled =
            checkOnEnableRoom()
        //.convertDateToLong() < System.currentTimeMillis()+70000 && System.currentTimeMillis() < home.endDate.convertDateToLong()
        finish_room?.isEnabled = checkOnEnableRoom()

        dataEndTextInputEditText?.isEnabled =
            dataStartTextInputEditText?.text.toString() != DEFAULT_STRING_DATA

        if (dataStartTextInputEditText?.text.toString().convertDateToLong() > dataEndTextInputEditText?.text.toString().convertDateToLong()) {
            Toast.makeText(context, "Проверьте время окончания аренды", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun checkOnEnableRoom() =
        (home.startDate != DEFAULT_STRING_DATA &&
                home.endDate != DEFAULT_STRING_DATA)


    private fun notifyChange() {
        val item = toolbar.menu.getItem(0)
        val s = SpannableString(getString(R.string.save))
        var color = ContextCompat.getColor(context!!, R.color.colorPrimary)
        s.setSpan(ForegroundColorSpan(color), 0, s.length, 0)
        item.title = s
        item.setOnMenuItemClickListener {
            updateHome()
            updateToolbarCode()
            notifyStateButton()
            sendDataToFirebase()
            color = ContextCompat.getColor(context!!, R.color.color_text_grey)
            s.setSpan(ForegroundColorSpan(color), 0, s.length, 0)
            item.title = s
            item.setOnMenuItemClickListener { true }
            true
        }
    }

    private fun closeDialog() {
        AlertDialog.Builder(context!!).setMessage("Завершить аренду?")
            .setPositiveButton("Завершить") { dialog, _ ->
                dataStartTextInputEditText.setText(DEFAULT_STRING_DATA)
                dataEndTextInputEditText.setText(DEFAULT_STRING_DATA)
                home.codeHome = 0
                updateDateHome()
                sendCodeToFirebase()
                sendDateToFirebase()
                updateToolbarCode()
                notifyChange()
                notifyStateButton()
                dialog.dismiss()
            }
            .setNeutralButton("Отмена") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun notifyUser() {
        var shareBody = "Для открытия двери введите код "
        shareBody += home.codeHome
        shareBody += " в приложении Quarta.\nВам предоставлен доступ с "
        shareBody += home.startDate
        shareBody += " до "
        shareBody += home.endDate
        shareBody += ".\nАдрес квартиры: "
        shareBody += home.address
        shareBody += ".\nОписание: "
        shareBody += home.description
        shareBody += ".\nскачать приложение Quarta\nдля iOS - https://vk.cc/9qpov1\nдля Android - https://vk.cc/9qppFF"
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Код доступа Гостя ${home.codeHome}")
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        showPopup(shareBody, home.codeHome.toString())
    }

    private fun showPopup(shareBody: String, key: String) {
        val bottomNavDrawerFragment = BottomNavigation(shareBody, key)
        bottomNavDrawerFragment.show(
            mainActivity.supportFragmentManager,
            bottomNavDrawerFragment.tag
        )

    }

    private fun updateDateHome() {
        home.startDate = dataStartTextInputEditText?.text.toString()
        home.endDate = dataEndTextInputEditText?.text.toString()
    }

    private fun updateHome() {
        if (dateIsModify()) {
            home.codeHome = Random().nextInt(899999) + 100000
        } //100 000 - 999 999
        updateDateHome()
        home.address = addressTextInputEditText?.text.toString()
        home.description = descriptionTextInputEditText?.text.toString()
    }

    private fun dateIsModify(): Boolean =
        home.startDate != dataStartTextInputEditText?.text.toString()

    private fun sendDataToFirebase() {
        val ref = FirebaseDatabase.getInstance().reference
        sendDateToFirebase()
        sendCodeToFirebase()
        ref.child("tasks").child(home.id).child("time").setValue(home.address)
        ref.child("tasks").child(home.id).child("opisaniye").setValue(home.description)
            .addOnSuccessListener {
                if (checkOnEnableRoom()) notifyUser()
            }.addOnCanceledListener {
                Toast.makeText(context, "Ошибка сохранения", Toast.LENGTH_SHORT).show()
            }
    }

    private fun sendCodeToFirebase() {
        if (previouslyCode != home.codeHome) {
            previouslyCode = home.codeHome
            val me = (mainActivity.application as App).currentUser
            val codes = Codes(
                flatId = home.id,
                name = me.name,
                address = home.address.toString(),
                boxNumber = home.boxNumber!!,
                code = home.codeHome.toString()
            )
            val time = System.currentTimeMillis()
            codes.date = time.convertLongToTime()
            codes.dateForFilter = time.convertLongToMonth()
            val ref = FirebaseDatabase.getInstance().reference
            ref
                .child("tasks")
                .child(home.id)
                .child("idHome")
                .setValue(home.codeHome)
            ref
                .child("users")
                .child(me.ownerId)
                .child("codes")
                .push()
                .setValue(codes.toMap())
        }
    }


    private fun sendDateToFirebase() {
        val ref = FirebaseDatabase.getInstance().reference
        ref.child("tasks").child(home.id).child("startDate").setValue(home.startDate)
        ref.child("tasks").child(home.id).child("endDate").setValue(home.endDate)
//        ref.child("tasks").child(home.flatId).child("lock").child("status")
//            .setValue(getCurrentValueEnabled())
    }

    private fun initListeners() {
        dataStartTextInputEditText?.apply {
            setOnClickListener { showCalendar(this, System.currentTimeMillis()) }
        }
        dataEndTextInputEditText?.apply {
            setOnClickListener {
                showCalendar(
                    this,
                    dataStartTextInputEditText?.text.toString().convertDateToLong() + 1000 * 60 * 60 * 24
                )
            }
        }
        addressTextInputEditText?.setOnFocusChangeListener { view, b ->
            if (b) {
                if (System.currentTimeMillis() - firstEditableFieldFocusedTime > 500) {
                    firstEditableFieldFocused = true
                    firstEditableFieldFocusedTime = System.currentTimeMillis()
                }
            }
        }
        descriptionTextInputEditText?.setOnClickListener {
            firstEditableFieldFocusedTime = System.currentTimeMillis()
        }
        descriptionTextInputEditText?.setOnFocusChangeListener { view, b ->
            if (b) {
                firstEditableFieldFocused = false
                firstEditableFieldFocusedTime = System.currentTimeMillis()
            }
        }
        addressTextInputEditText?.addTextChangedListener(listenerTextWatcher)
        descriptionTextInputEditText?.addTextChangedListener(listenerTextWatcher)
        finish_room?.setOnClickListener {
            closeDialog()
        }
        toolbar?.setNavigationOnClickListener {
            mainActivity.onBackPressed()
        }
        complete_text?.setOnClickListener {
            val inputMethodManager =
                activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager!!.hideSoftInputFromWindow(view!!.windowToken, 0)
        }

        KeyboardVisibilityEvent.setEventListener(activity!!) {
            if (it) {
                complete_text?.visibility = View.VISIBLE
                scroll_view?.fullScroll(View.FOCUS_DOWN)
                if (firstEditableFieldFocused) {
                    addressTextInputEditText?.requestFocus()
                } else
                    descriptionTextInputEditText?.requestFocus()
            } else
                complete_text?.visibility = View.GONE

        }
        notify_visitor?.setOnClickListener { notifyUser() }
    }

    companion object {
        fun newInstance(home: Home) = FragmentCurrentRoom(home)

    }
}
