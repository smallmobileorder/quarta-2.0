package com.quarta.android2.ui.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.quarta.android2.R
import com.quarta.android2.converToVisible
import com.quarta.android2.model.Codes
import kotlinx.android.synthetic.main.tab_codes.*
import java.util.*


open class FragmentCodesThisMonth : Fragment() {

    private lateinit var mainActivity: com.quarta.android2.MainActivity
    lateinit var mainFragment: FragmentListCodes
    private lateinit var adapterRecycler: AdapterCodes
    private var currentListPersonal = listOf<Codes>()
    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mainActivity.getListCodesFromFireBase(
            listenerSuccess = { list ->
                currentListPersonal = list
                updateRecycler(list)
                mainFragment.updateNumberText()
                pullUpRefresh?.isRefreshing = false
            },
            listenerFailed = {
                pullUpRefresh?.isRefreshing = false
                if (pullUpRefresh != null)
                    Snackbar.make(pullUpRefresh, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.tab_codes, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as com.quarta.android2.MainActivity
        initRecycler()
        pullUpRefresh?.setOnRefreshListener(refreshListener)
        pullUpRefresh?.isRefreshing = true
        refreshListener.onRefresh()
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.hideSoftInputFromWindow(view.windowToken, 0)

    }


    private fun initRecycler() {
        val data = mutableListOf<Codes>()
        adapterRecycler = AdapterCodes(data, mainActivity)
        val layoutManager = LinearLayoutManager(context)
        recycler_view?.layoutManager = layoutManager
        recycler_view?.adapter = adapterRecycler
    }


    private fun updateRecycler(list: List<Codes>) {
        under_recycler_text?.visibility = list.isEmpty().converToVisible()
        adapterRecycler.data = filter(list.toMutableList())
        adapterRecycler.notifyDataSetChanged()
    }

    open fun filter(list: MutableList<Codes>): MutableList<Codes> {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val currentMonth = year * 12 + month + 1
        return list.filter { it.getMonthFromCodes() == currentMonth }.toMutableList()
    }


    fun getSizeList(): String = adapterRecycler.data.size.toString()


    companion object {
        fun newInstance() = FragmentCodesThisMonth()
    }

}
