package com.quarta.android2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.FirebaseDatabase
import com.quarta.android2.R
import com.quarta.android2.model.Home
import com.quarta.android2.model.User
import kotlinx.android.synthetic.main.fragment_access_level.*


class FragmentAccessLevel(private val selectUser: User) : Fragment() {

    private lateinit var adapterRecycler: AdapterRoomsForAccess
    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        mainActivity.getListRoomFromFireBase(
            listenerSuccess = { list ->
                updateRecycler(list)
            },
            listenerFailed = {
                Snackbar.make(recycler_view, "Ошибка доступа", Snackbar.LENGTH_SHORT).show()
            })
    }

    private lateinit var mainActivity: com.quarta.android2.MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainActivity = activity as com.quarta.android2.MainActivity
        return inflater.inflate(R.layout.fragment_access_level, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initRecycler()
        initListeners()
        refreshListener.onRefresh()
    }

    private fun initViews() {
        btn_analytics?.isChecked = selectUser.isAnalyticEnabled
        btn_access_change_level?.isChecked = selectUser.isFlatEditEnabled
    }

    private fun initListeners() {
        btn_list_room?.setOnClickListener {
            if (adapterRecycler.listOwnerRooms.isEmpty()) {
                Snackbar.make(
                    recycler_view,
                    getString(R.string.empty_room_access_level),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                toggle()
            }
        }
        var ref = FirebaseDatabase.getInstance().reference
        ref = ref.child("users")
            .child(selectUser.id)
        btn_access_change_level.setOnCheckedChangeListener { _, isChecked ->
            ref.child("isFlatEditEnabled")
                .setValue(isChecked)
        }
        btn_analytics.setOnCheckedChangeListener { _, isChecked ->
            ref.child("isAnalyticEnabled")
                .setValue(isChecked)
        }
        toolbar?.setNavigationOnClickListener { mainActivity.onBackPressed() }
    }


    private fun initRecycler() {
        val data = mutableListOf<Home>()
        adapterRecycler = AdapterRoomsForAccess(data, selectUser, mainActivity)
        val layoutManager = LinearLayoutManager(context)
        recycler_view?.layoutManager = layoutManager
        recycler_view?.adapter = adapterRecycler
    }

    private var isExpand = true

    private fun toggle() {
        if (isExpand) {
            hide(recycler_view)
        } else {
            show(recycler_view)
        }
        isExpand = !isExpand
    }

    private fun hide(v: View?) {
        val animSlideDown = AnimationUtils.loadAnimation(context, R.anim.slide_up)
        animSlideDown.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                v?.visibility = View.INVISIBLE
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
        v?.startAnimation(animSlideDown)
    }

    private fun show(v: View?) {
        val animSlideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down)
        animSlideDown.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {
                v?.visibility = View.VISIBLE
            }

        })
        v?.startAnimation(animSlideDown)
    }

    private fun updateRecycler(list: List<Home>) {
        val data = list.toMutableList()
        data.sortBy { it.boxNumber }
        adapterRecycler.listOwnerRooms = data
        adapterRecycler.user = selectUser
        adapterRecycler.notifyDataSetChanged()
    }

}
