package com.quarta.android2

import android.annotation.SuppressLint
import android.view.View
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

@SuppressLint("SimpleDateFormat")
fun String.convertDateToLong(): Long {
    val df = SimpleDateFormat("dd.MM.yyyy HH:mm")
    return df.parse(this).time
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToTime(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd.MM.yyyy HH:mm")
    return format.format(date)
}

@SuppressLint("SimpleDateFormat")
fun Long.convertLongToMonth(): String {
    val date = Date(this)
    val format = SimpleDateFormat("MM.yyyy")
    return format.format(date)
}

fun Boolean?.converToVisible() =
    if (this == true) View.VISIBLE
    else View.GONE


@SuppressLint("SimpleDateFormat")
fun convertCalendarToData(date: Calendar): String {
    return String.format("%1\$td.%1\$tm.%1\$tY %1\$tH:%1\$tM", date)
}

fun String.emailCorrect(): Boolean {
    val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
    val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}