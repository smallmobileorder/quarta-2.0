package com.quarta.android2

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.quarta.android2.model.Analytics
import com.quarta.android2.model.Codes
import com.quarta.android2.model.Home
import com.quarta.android2.model.User
import com.quarta.android2.ui.fragments.FragmentListRoom
import com.quarta.android2.ui.fragments.FragmentLogin
import com.quarta.android2.ui.fragments.FragmentSplash
import kotlinx.android.synthetic.main.activity_main.*


@SuppressLint("Registered")
class MainActivity : AppCompatActivity() {

    var isAuth = false
    lateinit var resultFragment: Fragment
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()
        FirebaseApp.initializeApp(this)
        val login = getSharedPreferences("AUTH", Context.MODE_PRIVATE).getString("AUTH_EMAIL", "")
        val password =
            getSharedPreferences("AUTH", Context.MODE_PRIVATE).getString("AUTH_PASSWORD", "")
        if (login!!.isNotEmpty() && password!!.isNotEmpty()) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.container, FragmentSplash())
                .commitNow()
            trySignIn(login, password)
        } else {
            resultFragment = FragmentLogin.newInstance()
            replaceFragmentWithClearBackStack()
        }
    }

    private fun replaceFragmentWithClearBackStack() {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
        replaceFragment()
    }

    fun replaceFragment() {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            .replace(R.id.container, resultFragment)
            .addToBackStack(resultFragment.javaClass.name)
            .commitAllowingStateLoss()
    }

    fun logOut() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .requestProfile()
            .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient.signOut().addOnSuccessListener {
            logOutFragment()
        }
    }


    private fun logOutFragment() {
        resultFragment = FragmentLogin.newInstance()
        replaceFragmentWithClearBackStack()
        isAuth = false
        FirebaseAuth.getInstance().signOut()
        getSharedPreferences("AUTH", Context.MODE_PRIVATE).edit().putString("AUTH_EMAIL", "")
            .apply()
        getSharedPreferences("AUTH", Context.MODE_PRIVATE).edit().putString("AUTH_PASSWORD", "")
            .apply()
    }

    private fun trySignIn(login: String, password: String) {
        signIn(login, password) {
            if (it) {
                isAuth = true
                resultFragment = FragmentListRoom.newInstance()
                replaceFragment()
            } else {
                resultFragment = FragmentLogin.newInstance()
                replaceFragmentWithClearBackStack()
            }
        }
    }

    fun tryToAuth(email: String, password: String, callback: (Boolean) -> Unit) {
        signIn(email, password) {
            callback(it)
            if (it) {
                resultFragment = FragmentListRoom.newInstance()
                replaceFragmentWithClearBackStack()
                isAuth = true
                getSharedPreferences("AUTH", Context.MODE_PRIVATE)
                    .edit()
                    .putString("AUTH_EMAIL", email)
                    .apply()
                getSharedPreferences("AUTH", Context.MODE_PRIVATE)
                    .edit()
                    .putString("AUTH_PASSWORD", password)
                    .apply()
            } else {
                Snackbar.make(
                    container,
                    "Данный пользователь не найден. Возможно, он был удален.",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }


    private fun signIn(email: String, password: String, callback: (Boolean) -> Unit) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener { authResult ->
                getListUserFromFireBase(
                    listenerSuccess = {
                        val currentUser = it.find { user -> user.email == email }
                        if (currentUser == null) {
                            callback(false)
                        } else {
                            (application as App).currentUser = currentUser
                            callback(true)
                        }
                    },
                    listenerFailed = {
                        callback(false)
                    }
                )
            }
            .addOnFailureListener {
                signInWithLoginFromFirebase(email, password, callback)
            }
    }

    fun сreateUserUsingEmailAndPassowrd(
        user: User,
        callback: (Boolean) -> Unit
    ) {

        auth.createUserWithEmailAndPassword(user.email, user.password)
            .addOnSuccessListener { authResult ->
                user.id = authResult.user!!.uid
                createNewUser(
                    user = user,
                    listenerSuccess = {
                        (application as App).currentUser = it
                        callback(true)
                    },
                    listenerFailed = {
                        callback(false)
                    }
                )
            }
            .addOnFailureListener {
                callback(false)
            }
    }

    private fun createNewUser(
        user: User,
        listenerSuccess: (user: User) -> Unit,
        listenerFailed: () -> Unit
    ) {
        val parent = FirebaseDatabase.getInstance().reference
        user.ownerId = user.id
        parent
            .child("users")
            .child(user.id)
            .setValue(user.toMap()).addOnSuccessListener {
                listenerSuccess(user)
            }.addOnFailureListener {
                listenerFailed()
                Log.w(TAG, "getSession:onCancelled", it)
            }
    }


    private fun signInWithLoginFromFirebase(
        email: String,
        password: String,
        callback: (Boolean) -> Unit
    ) {
        getListUserFromFireBase(
            listenerSuccess = {
                val user = it.find { person ->
                    person.email == email
                            && person.password == password
                }
                if (user != null) {
                    (application as App).currentUser = user
                    callback(true)
                } else {
                    callback(false)
                }
            }, listenerFailed = {
                callback(false)
            }
        )
    }

    fun updateUserDate(
        listenerSuccess: (result: User) -> Unit,
        listenerFailed: () -> Unit
    ) {

        val parent = FirebaseDatabase.getInstance().reference
        parent
            .child("users")
            .child((application as App).currentUser.id)
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val data = dataSnapshot.value
                        if (data is Map<*, *>) {
                            val user = setUsersFieldsFromMap(
                                dataSnapshot.key!!,
                                data as Map<String, *>
                            )
                            println(user)
                            (application as App).currentUser = user
                            listenerSuccess(user)
                        } else listenerFailed()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        listenerFailed()
                        Log.w(TAG, "getSession:onCancelled", databaseError.toException())
                    }
                })
    }

    fun getListRoomFromFireBase(
        listenerSuccess: (result: MutableList<Home>) -> Unit,
        listenerFailed: () -> Unit
    ) {

        updateUserDate(listenerSuccess = {
            val parent = FirebaseDatabase.getInstance().reference
            parent.child("tasks")
                .addListenerForSingleValueEvent(
                    object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val data = dataSnapshot.value
                            if (data is Map<*, *>) {
                                listenerSuccess(collectRooms(data as Map<String, *>))
                            } else listenerFailed()
                        }

                        override fun onCancelled(databaseError: DatabaseError) {
                            listenerFailed()
                            Log.w(TAG, "getSession:onCancelled", databaseError.toException())
                        }
                    })
        }, listenerFailed = {
            listenerFailed()
        })
    }

    fun getListAnalyticsFromFireBase(
        listenerSuccess: (result: MutableList<Analytics>) -> Unit,
        listenerFailed: () -> Unit
    ) {

        val parent = FirebaseDatabase.getInstance().reference
        parent.child("users")
            .child((application as App).currentUser.ownerId)
            .child("analytic")
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val data = dataSnapshot.value
                        if (data is Map<*, *>) {
                            listenerSuccess(collectAnalytics(data as Map<String, *>))
                        } else listenerFailed()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        listenerFailed()
                        Log.w(TAG, "getSession:onCancelled", databaseError.toException())
                    }
                })
    }

    fun getListCodesFromFireBase(
        listenerSuccess: (result: MutableList<Codes>) -> Unit,
        listenerFailed: () -> Unit
    ) {

        val parent = FirebaseDatabase.getInstance().reference
        parent.child("users")
            .child((application as App).currentUser.ownerId)
            .child("codes")
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val data = dataSnapshot.value
                        if (data is Map<*, *>) {
                            listenerSuccess(collectCodes(data as Map<String, *>))
                        } else listenerFailed()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        listenerFailed()
                        Log.w(TAG, "getSession:onCancelled", databaseError.toException())
                    }
                })
    }


    fun getListUserFromFireBase(
        listenerSuccess: (result: MutableList<User>) -> Unit,
        listenerFailed: () -> Unit
    ) {

        val parent = FirebaseDatabase.getInstance().reference
        parent.child("users")
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val data = dataSnapshot.value
                        if (data is Map<*, *>) {
                            listenerSuccess(collectUsers(data as Map<String, *>))
                        } else listenerFailed()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        listenerFailed()
                        Log.w(TAG, "getSession:onCancelled", databaseError.toException())
                    }
                })
    }

    private fun collectRooms(mapRoom: Map<String, *>): MutableList<Home> {

        var listRoom = mutableListOf<Home>()

        mapRoom.forEach { (key, value) ->
            val singleRoom = value as Map<String, *>
            listRoom.add(setRoomFieldsFromMap(key, singleRoom)!!)
        }
        listRoom = listRoom.filter {
            (application as App).currentUser.listFlats.contains(it.id)
                    && it.boxNumber != null
                    && it.codeHome != null
        }.toMutableList()
        listRoom.sortBy { it.boxNumber }
        (mapRoom["landlordDoorsStatus"] as Map<String, *>).forEach { (user, value) ->
            (value as Map<String, *>).forEach { (t, u) ->
                if (u is Boolean) {
                    listRoom.find { it.id == t && user == it.userId }?.isOpen = u
                }
            }
        }
        return listRoom
    }

    private fun collectAnalytics(mapAnalytics: Map<String, *>): MutableList<Analytics> {

        val listAnalytics = mutableListOf<Analytics>()

        mapAnalytics.forEach { (key, value) ->
            val singleRoom = value as Map<String, *>
            listAnalytics.add(setAnalyticFieldsFromMap(singleRoom)!!)
        }
        listAnalytics.sortBy { -it.date.convertDateToLong() }
        return listAnalytics
    }

    private fun collectCodes(mapCodes: Map<String, *>): MutableList<Codes> {

        val listCodes = mutableListOf<Codes>()

        mapCodes.forEach { (key, value) ->
            val singleRoom = value as Map<String, *>
            listCodes.add(setCodesFieldsFromMap(singleRoom)!!)
        }
        listCodes.sortBy { -it.date.convertDateToLong() }
        return listCodes
    }

    private fun collectUsers(mapUsers: Map<String, *>): MutableList<User> {

        val listUsers = mutableListOf<User>()

        mapUsers.forEach { (key, value) ->
            val singleRoom = value as Map<String, *>
            listUsers.add(setUsersFieldsFromMap(key, singleRoom))
        }
        listUsers.sortBy { it.name }
        return listUsers
    }

    private fun setRoomFieldsFromMap(
        id: String,
        singleHome: Map<String, *>
    ): Home? {
        val resultHome = Home(id)
        resultHome.address = singleHome["time"].toString()///ВСЕ ВЕРНО!!!!
        resultHome.description = singleHome["opisaniye"].toString()
        resultHome.endDate = singleHome["endDate"].toString()
        resultHome.startDate = singleHome["startDate"].toString()
        resultHome.codeHome = singleHome["idHome"].toString().toIntOrNull()
        resultHome.boxNumber = singleHome["boxNumber"].toString().toIntOrNull()
        resultHome.userId = singleHome["user"].toString()
        val lock = singleHome["lock"] as Map<String, *>?
        if (lock != null) {
            resultHome.isEmpty = lock["status"].toString().toIntOrNull() == 0
        }
        return resultHome
    }

    private fun setAnalyticFieldsFromMap(
        singleAnalytics: Map<String, *>
    ): Analytics? {
        val resultAnalytics = Analytics()
        resultAnalytics.address = singleAnalytics["adress"].toString()
        resultAnalytics.name = singleAnalytics["name"].toString()
        resultAnalytics.date = singleAnalytics["date"].toString()
        resultAnalytics.flatId = singleAnalytics["flatId"].toString()
        resultAnalytics.boxNumber = singleAnalytics["boxNumber"].toString().toInt()
        resultAnalytics.isOpen = singleAnalytics["status"].toString().toBoolean()
        return resultAnalytics
    }

    private fun setCodesFieldsFromMap(
        singleCode: Map<String, *>
    ): Codes? {
        val resultCode = Codes()
        resultCode.flatId = singleCode["flatId"].toString()
        resultCode.address = singleCode["adress"].toString()
        resultCode.name = singleCode["name"].toString()
        resultCode.date = singleCode["date"].toString()
        resultCode.dateForFilter = singleCode["dateForFilter"].toString()
        resultCode.boxNumber = singleCode["boxNumber"].toString().toInt()
        resultCode.code = singleCode["idHome"].toString()
        return resultCode
    }

    private fun setUsersFieldsFromMap(
        id: String,
        singleUser: Map<String, *>
    ): User {
        val resultUser = User(id)
        resultUser.email = singleUser["email"].toString()
        resultUser.name = singleUser["name"].toString()
        if (singleUser["password"] != null)
            resultUser.password = singleUser["password"].toString()
        if (singleUser["ownerId"] != null)
            resultUser.ownerId = singleUser["ownerId"].toString()
        if (singleUser["isAnalyticEnabled"] != null)
            resultUser.isAnalyticEnabled = singleUser["isAnalyticEnabled"].toString().toBoolean()
        if (singleUser["isStaffEnabled"] != null)
            resultUser.isStaffEnabled = singleUser["isStaffEnabled"].toString().toBoolean()
        if (singleUser["isFlatEditEnabled"] != null)
            resultUser.isFlatEditEnabled = singleUser["isFlatEditEnabled"].toString().toBoolean()
        if (singleUser["flats"] != null)
            resultUser.listFlats = (singleUser["flats"] as Map<String, *>).keys.toMutableList()
        if (singleUser["staff"] != null) {
            resultUser.listStaffs = (singleUser["staff"] as Map<String, String>).toMutableMap()
        }
        return resultUser
    }

    companion object {
        private const val TAG = "GoogleActivity"
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }

}
