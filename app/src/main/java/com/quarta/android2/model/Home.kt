package com.quarta.android2.model

data class Home(
    val id: String,
    var boxNumber: Int? = 0,
    var startDate: String = "",
    var endDate: String = "",
    var address: String? = "",//time
    var description: String? = "",//opisaniye
    var codeHome: Int? = 0,
    var userId: String? = "",
    var isEmpty: Boolean = false,
    var isOpen: Boolean = false
) {
    companion object {
        const val DEFAULT_STRING_DATA = "00.00.0000 00:00"
    }

    fun roomIsNotEmpty(): Boolean =
        !roomIsEmpty(this.startDate) && !roomIsEmpty(this.endDate)

    private fun roomIsEmpty(it: String): Boolean =
        it.isEmpty() || it == DEFAULT_STRING_DATA
}