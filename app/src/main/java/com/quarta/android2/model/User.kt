package com.quarta.android2.model

data class User(
    var id: String,
    var email: String = "",
    var name: String = "",
    var password: String = "",
    var ownerId: String = "",
    var position: String = "",
    var isAnalyticEnabled: Boolean = false,
    var isStaffEnabled: Boolean = false,
    var isFlatEditEnabled: Boolean = false,
    var listFlats: MutableList<String> = mutableListOf(),
    var listStaffs: MutableMap<String, String> = mutableMapOf()
) {

    fun toMap(): Map<String, Any?> {
        val mutableMap = mutableMapOf<String, Any>()
        mutableMap["email"] = email
        mutableMap["name"] = name
        mutableMap["password"] = password
        mutableMap["ownerId"] = ownerId
        mutableMap["isAnalyticEnabled"] = isAnalyticEnabled
        mutableMap["isStaffEnabled"] = isStaffEnabled
        mutableMap["isFlatEditEnabled"] = isFlatEditEnabled
        return mutableMap

    }
}