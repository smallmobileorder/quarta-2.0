package com.quarta.android2.model

data class Codes(
    var flatId: String = "",
    var boxNumber: Int = 0,
    var date: String = "",
    var dateForFilter: String = "",
    var address: String = "",
    var name: String = "",
    var code: String = ""
) {
    fun getMonthFromCodes(): Int {
        val list = date.split(" ")[0].split(".")
        return list[1].toInt() + list[2].toInt() * 12
    }



    fun toMap(): Map<String, Any> {
        val mutableMap = mutableMapOf<String, Any>()
        mutableMap["adress"] = address
        mutableMap["boxNumber"] = boxNumber
        mutableMap["date"] = date
        mutableMap["dateForFilter"] = dateForFilter
        mutableMap["flatId"] = flatId
        mutableMap["idHome"] = code
        mutableMap["name"] = name
        return mutableMap

    }
}