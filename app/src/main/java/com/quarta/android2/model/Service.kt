package com.quarta.android2.model

data class Service(
    var email: String = "",
    var ownerId: String = "",
    var isDone: Boolean = false,
    var serviceType: ServiceType = ServiceType.CLEANING,
    var date: String = ""
) {

    fun toMap(): Map<String, Any?> {
        val mutableMap = mutableMapOf<String, Any>()
        mutableMap["email"] = email
        mutableMap["ownerId"] = ownerId
        mutableMap["isDone"] = isDone
        mutableMap["serviceType"] = serviceType.value
        mutableMap["date"] = date
        return mutableMap
    }
}

enum class ServiceType(val value: String) {
    CLEANING("cleaning"),
    TEXTIL("textile"),
    LAUNDRY("laundry"),
    REPAIR("repair"),
    CCTV("CCTV"),
    KEYLESSACCESS("keylessAccess"),
}
