package com.quarta.android2.model

data class Analytics(
    var address: String = "",
    var boxNumber: Int = 0,
    var date: String = "",
    var flatId: String = "",
    var name: String = "",
    var isOpen: Boolean = false
){
    fun toMap(): Map<String, Any> {
        val mutableMap = mutableMapOf<String, Any>()
        mutableMap["adress"] = address
        mutableMap["boxNumber"] = boxNumber
        mutableMap["date"] = date
        mutableMap["flatId"] = flatId
        mutableMap["name"] = name
        mutableMap["status"] = isOpen
        return mutableMap
    }
}